---
title: Sign up
subtitle: Join the IBC's Alumni Association
comments: false
---

<html lang="en-US">
    <!-- Begin Mailchimp Signup Form -->
    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        #mc_embed_signup {
            background: #fff;
            clear: left;
            font: 14px Helvetica, Arial, sans-serif;
        }
        #mc_embed_signup div#mce-responses {
            padding: 0 !important;
            margin: 0 !important;
        }
        .mc-field-group input {
            padding: 8px 0 8px 8px !important;
            text-indent: 0 !important;
        }
        .interestgroup_field {
            display: flex;
            flex-wrap: wrap;
            justify-content: flex-start;
            padding-left: 0 !important;
            margin-top: 10px !important;
        }
        .interestgroup_field > li {
            flex: 50%;
         }
        .post__content ul li::before {
            content: none !important;
        }
    </style>
    <!-- Begin Mailchimp Signup Form -->
    <div id="mc_embed_signup">
        <form action="https://ibcaa.us1.list-manage.com/subscribe/post?u=c5367e13083a9a4393c8aa141&amp;id=164d5620b3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
                <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                <div class="mc-field-group">
                    <label for="mce-FNAME">First Name <span class="asterisk">*</span></label>
                    <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
                </div>
                <div class="mc-field-group">
                    <label for="mce-LNAME">Last Name <span class="asterisk">*</span></label>
                    <input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
                </div>
                <div class="mc-field-group">
                    <label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                </div>
                <div class="mc-field-group size1of2">
                    <label for="mce-PHONE">Phone Number </label>
                    <input type="text" name="PHONE" class="" value="" id="mce-PHONE">
                </div>
                <div class="mc-field-group">
                    <label for="mce-LINKEDIN">LinkedIn Url </label>
                    <input type="url" value="" name="LINKEDIN" class=" url" id="mce-LINKEDIN">
                </div>
                <div class="mc-field-group size1of2">
                    <label for="mce-GRADYEAR">Graduation Year  <span class="asterisk">*</span></label>
                    <input type="number" name="GRADYEAR" class="required" value="" id="mce-GRADYEAR">
                </div>
                <div class="mc-field-group">
                    <label for="mce-MAJOR">Major <span class="asterisk">*</span></label>
                    <input type="text" value="" name="MAJOR" class="required" id="mce-MAJOR">
                </div>
                <div class="mc-field-group input-group">
                    <strong>Active Semesters in IBC </strong>
                    <ul class="interestgroup_field">
                        <li>
                            <input type="checkbox" value="1" name="group[378290][1]" id="mce-group[378290]-378290-0">
                            <label for="mce-group[378290]-378290-0">FA 2013</label>
                        </li>
                        <li>
                            <input type="checkbox" value="4" name="group[378290][4]" id="mce-group[378290]-378290-1">
                            <label for="mce-group[378290]-378290-1">SP 2014</label>
                        </li>
                        <li>
                            <input type="checkbox" value="8" name="group[378290][8]" id="mce-group[378290]-378290-2">
                            <label for="mce-group[378290]-378290-2">FA 2014</label>
                        </li>
                        <li>
                            <input type="checkbox" value="16" name="group[378290][16]" id="mce-group[378290]-378290-3">
                            <label for="mce-group[378290]-378290-3">SP 2015</label>
                        </li>
                        <li>
                            <input type="checkbox" value="32" name="group[378290][32]" id="mce-group[378290]-378290-4">
                            <label for="mce-group[378290]-378290-4">FA 2015</label>
                        </li>
                        <li>
                            <input type="checkbox" value="64" name="group[378290][64]" id="mce-group[378290]-378290-5">
                            <label for="mce-group[378290]-378290-5">SP 2016</label>
                        </li>
                        <li>
                            <input type="checkbox" value="128" name="group[378290][128]" id="mce-group[378290]-378290-6">
                            <label for="mce-group[378290]-378290-6">FA 2016</label>
                        </li>
                        <li>
                            <input type="checkbox" value="256" name="group[378290][256]" id="mce-group[378290]-378290-7">
                            <label for="mce-group[378290]-378290-7">SP 2017</label>
                        </li>
                        <li>
                            <input type="checkbox" value="512" name="group[378290][512]" id="mce-group[378290]-378290-8">
                            <label for="mce-group[378290]-378290-8">FA 2017</label>
                        </li>
                        <li>
                            <input type="checkbox" value="1024" name="group[378290][1024]" id="mce-group[378290]-378290-9">
                            <label for="mce-group[378290]-378290-9">SP 2018</label>
                        </li>
                        <li>
                            <input type="checkbox" value="2048" name="group[378290][2048]" id="mce-group[378290]-378290-10">
                            <label for="mce-group[378290]-378290-10">FA 2018</label>
                        </li>
                        <li>
                            <input type="checkbox" value="4096" name="group[378290][4096]" id="mce-group[378290]-378290-11">
                            <label for="mce-group[378290]-378290-11">SP 2019</label>
                        </li>
                        <li>
                            <input type="checkbox" value="8192" name="group[378290][8192]" id="mce-group[378290]-378290-12">
                            <label for="mce-group[378290]-378290-12">FA 2019</label>
                        </li>
                        <li>
                            <input type="checkbox" value="16384" name="group[378290][16384]" id="mce-group[378290]-378290-13">
                            <label for="mce-group[378290]-378290-13">SP 2020</label>
                        </li>
                        <li>
                            <input type="checkbox" value="32768" name="group[378290][32768]" id="mce-group[378290]-378290-14">
                            <label for="mce-group[378290]-378290-14">FA 2020</label>
                        </li>
                        <li>
                            <input type="checkbox" value="65536" name="group[378290][65536]" id="mce-group[378290]-378290-15">
                            <label for="mce-group[378290]-378290-15">SP 2021</label>
                        </li>
                    </ul>
                </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    
                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                    <input type="text" name="b_c5367e13083a9a4393c8aa141_164d5620b3" tabindex="-1" value="">
                </div>
                <div class="clear">
                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                </div>
            </div>
        </form>
    </div>
    <!--End mc_embed_signup-->
    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
    <script type='text/javascript'>
        (function ($) { 
            window.fnames = new Array();
            window.ftypes = new Array();
            fnames[0] = 'MERGE0';
            ftypes[0] = 'email';
            fnames[1] = 'FNAME';
            ftypes[1] = 'text';
            fnames[2] = 'LNAME';
            ftypes[2] = 'text';
            fnames[3] = 'ADDRESS';
            ftypes[3] = 'address';
            fnames[4] = 'PHONE';
            ftypes[4] = 'phone';
            fnames[5] = 'BIRTHDAY';
            ftypes[5] = 'birthday';
        }(jQuery));
        var $mcj = jQuery.noConflict(true);
    </script>
</html>
